<a name="1.0.0"></a>
# 1.0.0 (2018-05-29)


### Bug Fixes

* **StockOut:** 修复gitignore ([9f4676e](https://gitee.com/jsqf/yyb_demo/commits/9f4676e))


### Features

* **StockIn:** add changelog ([61bcf7c](https://gitee.com/jsqf/yyb_demo/commits/61bcf7c))
* **StockIn:** 增加事务控制 ([7cf4b8f](https://gitee.com/jsqf/yyb_demo/commits/7cf4b8f))
* **StockOut:** 增加新功能 ([1df93e9](https://gitee.com/jsqf/yyb_demo/commits/1df93e9))



