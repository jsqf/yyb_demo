package com.yanzhan.service;

import com.yanzhan.entity.User;

public interface UserService {
    public User selectUserById(Integer userId);
    void insertUser(User user);
    void esTest();
}
