package com.yanzhan.service.impl;

import com.yanzhan.dao.UserDAO;
import com.yanzhan.entity.User;
import com.yanzhan.service.UserService;
import com.yanzhan.util.EsClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("UserService")
public class UserServiceImpl implements UserService{
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private EsClientBuilder client;
    @Cacheable(value="myCache",key="#userId")
    public User selectUserById(Integer userId) {
        return userDAO.selectUserById(userId);
    }

    @CacheEvict(value="myCache",key="#user.userEmail",beforeInvocation=true)
    @Transactional
    @Override
    public void insertUser(User user) {
        userDAO.insertUser(user);
//        throw new RuntimeException("test");
    }

    @Override
    public void esTest() {
        client.searchSingle();
    }
}
