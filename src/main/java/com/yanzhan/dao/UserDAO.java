package com.yanzhan.dao;

import com.yanzhan.entity.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UserDAO{
    public User selectUserById(Integer userId);
    void insertUser(User user);
}
