package com.yanzhan.controller;

import com.yanzhan.RSAUtil.RSAEncrypt;
import com.yanzhan.entity.User;
import com.yanzhan.service.UserService;
import com.yanzhan.util.RSAUtil1;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.codec.binary.Base64;

@RestController
@RequestMapping(value = "/user")
public class UserController {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private UserService userService;

    @ApiOperation(value = "获取用户")
    @RequestMapping(value = "getUser", method = RequestMethod.GET)
    public Map<String, Object> getUser(@ApiParam(value = "用户ID", required = true)@RequestParam int id){
        Map<String, Object> map = new HashMap<String, Object>();
        logger.info("////////////////");
//        System.out.println(userService.selectUserById(1));
        try{
            map.put("data", userService.selectUserById(id));
            map.put("status", 1);
        }catch (Exception e){
            e.printStackTrace();
            map.put("status", 0);
            map.put("msg", "error:" + e.getMessage());
            logger.error("user get error", id);
        }
        return map;
    }

    @ApiOperation(value = "插入用户")
    @RequestMapping(value = "insertUser", method = RequestMethod.GET)
    public Map<String, Object> insertUser(){
        Map<String, Object> map = new HashMap<String, Object>();
        logger.info("////////////////");
        try{
            User user = new User();
            user.setUserId(400);
            user.setUserPassword("111");
            user.setUserName("xxx");
            userService.insertUser(user);
            map.put("status", 1);
        }catch (Exception e){
            e.printStackTrace();
            map.put("status", 0);
            map.put("msg", "error:" + e.getMessage());
        }
        return map;
    }

    @ApiOperation(value = "测试ES")
    @RequestMapping(value = "esTest", method = RequestMethod.GET)
    public Map<String, Object> esTest(){
        Map<String, Object> map = new HashMap<String, Object>();
        logger.info("////////////////");
        try{
//            map.put("data", userService.selectUserById(1));
            userService.esTest();
            map.put("status", 1);
        }catch (Exception e){
            e.printStackTrace();
            map.put("status", 0);
            map.put("msg", "error:" + e.getMessage());
        }
        return map;
    }


    @ApiOperation(value = "login")
    @RequestMapping(value = "login", method = RequestMethod.GET)
    public Map<String, Object> login(@ApiParam(value = "用户ID", required = true)@RequestParam String encrypted){
        Map<String, Object> map = new HashMap<>();
        try {
            byte[] en_result = new BigInteger(encrypted, 16).toByteArray();
            //System.out.println("转成byte[]" + new String(en_result));
            byte[] de_result = RSAUtil1.decrypt(RSAUtil1.getKeyPair().getPrivate(),
                    en_result);
            System.out.println("还原密文：");
            String xx = new String(de_result);
            StringBuffer sb = new StringBuffer();
            for(int i = xx.toCharArray().length-1; i>=0; i--){
                sb.append(xx.charAt(i));
            }
            String minwen = new String(sb);
            System.out.println(minwen);
            String[] ccc = minwen.split("%23");


            if("wangjun".equalsIgnoreCase(ccc[0]) && "wangjun123".equalsIgnoreCase(ccc[1])){
                long time = Long.parseLong(ccc[2]);
                long now_data = new Date().getTime();
                //1分钟超时
                if((now_data - time)/ 60000 > 10){
                    //返回错误页面
                    map.put("status", 0);
                    map.put("msg", "URL失效");
                }else {
                    map.put("status", 0);
                    map.put("msg", "ok");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 0);
            map.put("msg", "error bug");
        }
        return map;
    }

    @ApiOperation(value = "RSA加密测试")
    @RequestMapping(value = "login1", method = RequestMethod.GET)
    public Map<String, Object> login1(@ApiParam(value = "用户ID", required = true)@RequestParam String encrypted) {
        Map<String, Object> map = new HashMap<>();
//        String filepath = "/Users/yyb/Downloads/";
        String filepath = null;
        Base64 base64 = new Base64();
        byte[] res = new byte[0];
        try {

            res = RSAEncrypt.decrypt(RSAEncrypt.loadPrivateKeyByStr(RSAEncrypt.loadPrivateKeyByFile(filepath)), base64.decode(encrypted));
        } catch (Exception e) {
            e.printStackTrace();
        }
        String restr = new String(res);
        map.put("data", restr);
        map.put("status", 1);
        return map;
    }


}
